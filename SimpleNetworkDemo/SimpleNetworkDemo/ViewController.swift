//
//  ViewController.swift
//  SimpleNetworkDemo
//
//  Created by Gopal Nair on 2/13/15.
//  Copyright (c) 2015 Gopal Nair. All rights reserved.
//

import UIKit
import Foundation;

class ViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    //STEP1 : Create a NSURL object.
    let url = NSURL(string: "http://www.worldofwarcraft.com");
    
    //STEP 2: Get a handle to shred session
    var sharedSession = NSURLSession.sharedSession();
    
    //In this example, we will send a server request to apexheli.com, when the application starts. 
    //We will then display the response in the webview.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        //STEP 3: Create a task, and parse contents in completion handler.
        var task = sharedSession.dataTaskWithURL(url!, completionHandler: { (data, response, error) -> Void in
            
            if(error == nil)
            {
                
                //The returned data is in encoded format. Here is how we convert it to string.
                let strContent = NSString(data: data, encoding: NSUTF8StringEncoding);
                println(strContent);
                
                //As part of completion handler, load the HTML to webview. Typically, the content we get back will be JSON/XML, and we will have parsing logic here.
                self.webView.loadHTMLString(strContent, baseURL: nil);
                
            }
            
            
        })
        
        task.resume();
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

