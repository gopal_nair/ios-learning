//
//  ViewController.swift
//  QRCode Generator
//
//  Created by Gopal Nair on 2/14/15.
//  Copyright (c) 2015 Gopal Nair. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputText: UITextField!
    @IBOutlet weak var qrImage: UIImageView!
    
    
    let strApiKey:String    = "JzVhasdeZBmshGDMaO1CIllOYZN2p1bwZKojsnqXDWaaJ1lQ6e";
    let endPointURL:String  = "https://mutationevent-qr-code-generator.p.mashape.com/generate.php";
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func generatePress(sender: UIButton) {
        
        //End any editing and retire the keyboard.
        self.view.endEditing(true);
        
        //If the input text is empty, then show an error message
        if(inputText.text == "")
        {
            let alertView = UIAlertController(title: "Text Cannot be blank", message: "Empty Text", preferredStyle: UIAlertControllerStyle.Alert)
            alertView.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alertView, animated: true, completion: nil)
            return;
        }
        
        //Send data to server and get QR code back.
        sendQRRequest(inputText.text, pImage: self.qrImage)

    }
    
    
    func sendQRRequest(txtData:String, pImage:UIImageView)
    {
        
        var headerValues:NSDictionary = ["X-Mashape-Key":"JzVhasdeZBmshGDMaO1CIllOYZN2p1bwZKojsnqXDWaaJ1lQ6e", "content":txtData, "quality":"H", "size":"6", "type":"text"];
        
        let endUrl = endPointURL+"?content="+txtData.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!;
        
        println("Encoded URL : \(endUrl)");
        let strUrl = NSURL(string: endUrl);
        var objRequest = NSMutableURLRequest(URL: strUrl!);
        
        //Set Mashape API Key
        //ob.setValue(value: "JzVhasdeZBmshGDMaO1CIllOYZN2p1bwZKojsnqXDWaaJ1lQ6e", forKey: "X-Mashape-Key")
        objRequest.setValue("JzVhasdeZBmshGDMaO1CIllOYZN2p1bwZKojsnqXDWaaJ1lQ6e", forHTTPHeaderField: "X-Mashape-Key")
        objRequest.setValue(txtData, forHTTPHeaderField: "content")
        objRequest.setValue("H", forHTTPHeaderField: "quality")
        objRequest.setValue("6", forHTTPHeaderField: "size")
        objRequest.setValue("text", forHTTPHeaderField: "type")
        
        
//        //STEP 3: Create a task, and parse contents in completion handler.
//        var task = NSURLSession.sharedSession().dataTaskWithURL(strUrl!, completionHandler: { (data, response, error) -> Void in
//            
//            if(error == nil)
//            {
//                
//                //The returned data is in encoded format. Here is how we convert it to string.
//                let strContent = NSString(data: data, encoding: NSUTF8StringEncoding);
//                println(strContent);
//                
//            }
//            
//        })
        
//        task.resume();
        
        
        var task = NSURLSession.sharedSession().dataTaskWithRequest(objRequest, completionHandler: { (data, response, error) -> Void in
            if(error == nil)
                        {
            
                            //The returned data is in encoded format. Here is how we convert it to string.
                            let strContent = NSString(data: data, encoding: NSUTF8StringEncoding);
                            println(strContent);
                            
                            
                        }
            else
            {
                println("Error Encountered");
            }
        })
        
        task.resume();
        
        
    } //func sendQRRequest(txtData:String, pImage:UIImageView)
    

}

